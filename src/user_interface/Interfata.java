package user_interface;
import java.awt.EventQueue;
import java.sql.*;
import java.awt.CardLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.awt.event.ActionEvent;
import bll.ClientBLL;
import bll.ComandaBLL;
import bll.ProdusBLL;
import connection.SQL_connection;
import net.proteanit.sql.DbUtils;
import start.CreareBon;
import model.Client;
import model.Produs;
import model.Comanda;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.Font;
public class Interfata {
	private JFrame frame;
	private JPanel Meniu;
	private JPanel Clienti;
	private JPanel Produse;
	private JPanel Comanda;
	private JTable table;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfata window = new Interfata();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfata() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	Connection connection=null;
	private JTextField prod1;
	private JTextField prod2;
	private JTextField prod3;
	private JTextField prod4;
	private JTable table_1;
	private JTextField cl1;
	private JTextField cl2;
	private JTextField cl3;
	private JTextField cl4;
	private JTextField cl5;
	private JTable table_2;
	private JTable table_3;
	private JTable table_4;
	private JTextField cmd1;
	private JTextField cmd2;
	private JTextField cmd3;
	private JTextField cmd4;
	private JTextField cmd5;
	private void initialize() {
		connection=SQL_connection.getConnection();
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 463);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		
		final JPanel Meniu = new JPanel();
		frame.getContentPane().add(Meniu, "name_93399140098025");
		Meniu.setLayout(null);
		
		final JPanel Produse = new JPanel();
		frame.getContentPane().add(Produse, "name_93403376843311");
		Produse.setLayout(null);
		
		final JPanel Comanda = new JPanel();
		frame.getContentPane().add(Comanda, "name_93404885982697");
		Comanda.setLayout(null);

		final JPanel Clienti = new JPanel();
		frame.getContentPane().add(Clienti, "name_93401489892279");
		Clienti.setLayout(null);
		
		//////////////////////////////////////////////// CLIENTI
		
		JButton btnClienti = new JButton("Clienti");
		btnClienti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Clienti.setVisible(true);
				Meniu.setVisible(false);
				try {
					String query="select * from client";
					PreparedStatement pst=connection.prepareStatement(query);
					ResultSet rs=pst.executeQuery();
					table_1.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnClienti.setBounds(537, 174, 226, 152);
		Meniu.add(btnClienti);
		
		JButton btnUpdate_1 = new JButton("Update");
		btnUpdate_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id=Integer.parseInt(cl1.getText());
				String nume=cl2.getText();
				String adresa=cl3.getText();
				String email=cl4.getText();
				int varsta=Integer.parseInt(cl5.getText());
				ClientBLL x=new ClientBLL();
				id=x.updateClient(id, nume, adresa, email, varsta);
				try {
					String query="select * from client";
					PreparedStatement pst=connection.prepareStatement(query);
					ResultSet rs=pst.executeQuery();
					table_1.setModel(DbUtils.resultSetToTableModel(rs));
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				if(id>-1)
					JOptionPane.showMessageDialog(null,"Detaliile despre client au fost editate cu succes!"," ",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		btnUpdate_1.setBounds(40, 228, 206, 23);
		Clienti.add(btnUpdate_1);
	
		JButton btnStergere_1 = new JButton("Stergere");
		btnStergere_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id;
				int id_curent=Integer.parseInt(cl1.getText());
				ClientBLL x=new ClientBLL();
				id=x.stergeClient(id_curent);
				try {
					String query="select * from client";
					PreparedStatement pst=connection.prepareStatement(query);
					ResultSet rs=pst.executeQuery();
					table_1.setModel(DbUtils.resultSetToTableModel(rs));
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				if(id>-1)
					JOptionPane.showMessageDialog(null,"Clientul a fost sters cu succes!"," ",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		btnStergere_1.setBounds(40, 296, 206, 23);
		Clienti.add(btnStergere_1);
		
		JButton btnAdaugare_1 = new JButton("Adaugare");
		btnAdaugare_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id;
				String nume=cl2.getText();
				String adresa=cl3.getText();
				String email=cl4.getText();
				int varsta=Integer.parseInt(cl5.getText());
				Client clientNou=new Client(nume,adresa,email,varsta);
				ClientBLL c=new ClientBLL();
				id=c.insertClient(clientNou);
				try {
					String query="select * from client";
					PreparedStatement pst=connection.prepareStatement(query);
					ResultSet rs=pst.executeQuery();
					table_1.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				if(id>-1)
					JOptionPane.showMessageDialog(null,"Clientul a fost adaugat cu succes!","",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		btnAdaugare_1.setBounds(40, 262, 206, 23);
		Clienti.add(btnAdaugare_1);
		////////////////////////////////////////////// END CLIENTI
		
		////////////////////////////////////////////// COMANDA
		JButton btnComanda = new JButton("Comanda");
		btnComanda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Comanda.setVisible(true);
				Meniu.setVisible(false);
				try {
					String query="select * from client";
					PreparedStatement pst=connection.prepareStatement(query);
					ResultSet rs=pst.executeQuery();
					table_2.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				try {
					String query="select * from product";
					PreparedStatement pst=connection.prepareStatement(query);
					ResultSet rs=pst.executeQuery();
					table_3.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				try {
					String query="select * from comanda";
					PreparedStatement pst=connection.prepareStatement(query);
					ResultSet rs=pst.executeQuery();
					table_4.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				table_2.addMouseListener(new MouseAdapter(){ // selectie client din tabela client
				    public void mouseClicked(MouseEvent evnt) {
				        if (evnt.getClickCount() == 1) {
				            int x=(int) table_2.getValueAt(table_2.getSelectedRow(),0);
				            cmd1.setText(Integer.toString(x));
				            String y=(String)table_2.getValueAt(table_2.getSelectedRow(),1);
				            cmd2.setText(y);
				         }
				     }
				});
				table_3.addMouseListener(new MouseAdapter(){ //selectie produs din tabela produs
				    public void mouseClicked(MouseEvent evnt) {
				        if (evnt.getClickCount() == 1) {
				            int x=(int) table_3.getValueAt(table_3.getSelectedRow(),0);
				            cmd3.setText(Integer.toString(x));
				            String y=(String)table_3.getValueAt(table_3.getSelectedRow(),1);
				            cmd4.setText(y);
				         }
				     }
				});
			}
		});
		
		JButton btnAdaugareProdus = new JButton("Adaugare comanda");
		btnAdaugareProdus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id_client=Integer.parseInt(cmd1.getText());
				//int nume_client=Integer.parseInt(cmd2.getText());
				int id_produs=Integer.parseInt(cmd3.getText());
				//int nume_produs=Integer.parseInt(cmd4.getText());
				int cantitate=Integer.parseInt(cmd5.getText());
				int pret,pret_total,id,cantitate_noua,mem;
				String nume_client,nume_produs;
				ProdusBLL x=new ProdusBLL();
				int aux=x.gasireProdus(id_produs).getCantitate();
				if(aux<cantitate){
					JOptionPane.showMessageDialog(null,"Stock insuficient!","",JOptionPane.INFORMATION_MESSAGE);
				}
				else{
					Produs p=new Produs();
					Client c=new Client();
					ClientBLL cl=new ClientBLL();
					p=x.gasireProdus(id_produs);
					c=cl.gasireClient(id_client);
					pret=p.getPret();
					pret_total=pret*cantitate;
					Comanda cmd=new Comanda(id_client,id_produs,cantitate,pret_total);
					ComandaBLL cmd1=new ComandaBLL();
					id=cmd1.inserareComanda(cmd);
					cantitate_noua=p.getCantitate()-cantitate;
					x.updateCantitate(id_produs, cantitate_noua);
					nume_client=c.getNume();
					nume_produs=p.getNume();
					mem=id_client;
					try {
						String query="select * from client";
						PreparedStatement pst=connection.prepareStatement(query);
						ResultSet rs=pst.executeQuery();
						table_2.setModel(DbUtils.resultSetToTableModel(rs));
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					try {
						String query="select * from product";
						PreparedStatement pst=connection.prepareStatement(query);
						ResultSet rs=pst.executeQuery();
						table_3.setModel(DbUtils.resultSetToTableModel(rs));
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					try {
						String query="select * from comanda";
						PreparedStatement pst=connection.prepareStatement(query);
						ResultSet rs=pst.executeQuery();
						table_4.setModel(DbUtils.resultSetToTableModel(rs));
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					if(id>-1){
						JOptionPane.showMessageDialog(null,"Comanda a fost efectuata!","",JOptionPane.INFORMATION_MESSAGE);
						CreareBon.CreareBon(nume_client, nume_produs, pret_total);
					}
				}
			}
		});
		btnAdaugareProdus.setBounds(28, 312, 158, 45);
		Comanda.add(btnAdaugareProdus);
		
		
		/*ListSelectionModel model=table_2.getSelectionModel();
		model.addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent e){
				if(!model.isSelectionEmpty()){
					int index=model.getMinSelectionIndex();
					int client_selectat=Integer.parseInt(table_2.getValueAt(index,0).toString());
					String x="Clientul a fost adaugat cu succes!";
					x=x+Integer.toString(client_selectat);
					JOptionPane.showMessageDialog(null,x,"",JOptionPane.INFORMATION_MESSAGE);
					
				}
			}
		});*/
		btnComanda.setBounds(274, 174, 226, 152);
		Meniu.add(btnComanda);
		//////////////////////////////////////////////// END COMANDA
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Clienti.setVisible(false);
				Meniu.setVisible(true);
			}
		});
		btnBack.setBounds(68, 350, 644, 52);
		Clienti.add(btnBack);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(283, 38, 473, 281);
		Clienti.add(scrollPane_1);
		
		table_1 = new JTable();
		scrollPane_1.setViewportView(table_1);
		
		cl1 = new JTextField();
		cl1.setBounds(107, 38, 139, 20);
		Clienti.add(cl1);
		cl1.setColumns(10);
		
		cl2 = new JTextField();
		cl2.setBounds(107, 71, 139, 20);
		Clienti.add(cl2);
		cl2.setColumns(10);
		
		cl3 = new JTextField();
		cl3.setBounds(107, 102, 139, 20);
		Clienti.add(cl3);
		cl3.setColumns(10);
		
		cl4 = new JTextField();
		cl4.setBounds(107, 133, 139, 20);
		Clienti.add(cl4);
		cl4.setColumns(10);
		
		cl5 = new JTextField();
		cl5.setBounds(107, 164, 139, 20);
		Clienti.add(cl5);
		cl5.setColumns(10);
		
		JLabel lblId = new JLabel("ID client");
		lblId.setBounds(40, 41, 46, 14);
		Clienti.add(lblId);
		
		JLabel lblNume_1 = new JLabel("Nume");
		lblNume_1.setBounds(40, 74, 46, 14);
		Clienti.add(lblNume_1);
		
		JLabel lblVarsta = new JLabel("Adresa");
		lblVarsta.setBounds(40, 105, 46, 14);
		Clienti.add(lblVarsta);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(40, 136, 46, 14);
		Clienti.add(lblEmail);
		
		JLabel lblNrTel = new JLabel("Varsta");
		lblNrTel.setBounds(40, 167, 46, 14);
		Clienti.add(lblNrTel);
		
		
		//////////////////////////////////////// PRODUSE
		JButton btnProduse = new JButton("Produse");
		btnProduse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Produse.setVisible(true);
				Meniu.setVisible(false);
				table.addMouseListener(new MouseAdapter(){ // selectie client din tabela client
				    public void mouseClicked(MouseEvent evnt) {
				        if (evnt.getClickCount() == 1) {
				            int x=(int) table.getValueAt(table.getSelectedRow(),0);
				            prod1.setText(Integer.toString(x));
				            int y=(int)table.getValueAt(table.getSelectedRow(),2);
				            prod3.setText(Integer.toString(y));
				            float z=(float) table.getValueAt(table.getSelectedRow(),3);
				            prod2.setText(Float.toString(z));
				            String w=(String)table.getValueAt(table.getSelectedRow(),1);
				            prod4.setText(w);
				         }
				     }
				});
			}
		});
		btnProduse.setBounds(10, 174, 226, 152);
		Meniu.add(btnProduse);
		
		JLabel lblOnlineShop = new JLabel("ONLINE SHOP");
		lblOnlineShop.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblOnlineShop.setBounds(326, 37, 305, 60);
		Meniu.add(lblOnlineShop);
		
		JButton btnAdaugare = new JButton("Adaugare");
		btnProduse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String query="select * from product";
					PreparedStatement pst=connection.prepareStatement(query);
					ResultSet rs=pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnAdaugare.setBounds(22, 249, 210, 23);
		Produse.add(btnAdaugare);
		btnAdaugare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id;
				String nume=prod4.getText();
				int cantitate=Integer.parseInt(prod2.getText());
				int pret=Integer.parseInt(prod3.getText());
				Produs produsNou=new Produs(nume,cantitate,pret);
				ProdusBLL x=new ProdusBLL();
				id=x.inserareProdus(produsNou);
				if(id>-1)
					JOptionPane.showMessageDialog(null,"Produsul a fost adaugat cu succes!"," ",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id=Integer.parseInt(prod1.getText());
				int cantitate=Integer.parseInt(prod2.getText());
				int pret=Integer.parseInt(prod3.getText());
				String nume=prod4.getText();
				Produs product=new Produs(id,nume,cantitate,pret);
				ProdusBLL x=new ProdusBLL();
				id=x.updateProdus(product);
				if(id>-1)
					JOptionPane.showMessageDialog(null,"Produsul a fost editat cu succes!","",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		JButton btnStergere = new JButton("Stergere");
		btnStergere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id;
				int id_curent=Integer.parseInt(prod1.getText());
				ProdusBLL x=new ProdusBLL();
				id=x.stergeProdus(id_curent);
				try {
					String query="select * from product";
					PreparedStatement pst=connection.prepareStatement(query);
					ResultSet rs=pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				if(id>-1)
					JOptionPane.showMessageDialog(null,"Produsul a fost sters cu succes!","",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		JButton btnBack_1 = new JButton("Back");
		btnBack_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Produse.setVisible(false);
				Meniu.setVisible(true);
			}
		});
		btnBack_1.setBounds(46, 351, 684, 49);
		Produse.add(btnBack_1);
		/////////////////////////////////////////// END PRODUSE
		btnStergere.setBounds(22, 296, 210, 23);
		Produse.add(btnStergere);
		
		btnUpdate.setBounds(22, 205, 210, 23);
		Produse.add(btnUpdate);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(264, 35, 497, 284);
		Produse.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		prod1 = new JTextField();
		prod1.setBounds(81, 53, 151, 20);
		Produse.add(prod1);
		prod1.setColumns(10);
		
		prod2 = new JTextField();
		prod2.setBounds(81, 84, 151, 20);
		Produse.add(prod2);
		prod2.setColumns(10);
		
		prod3 = new JTextField();
		prod3.setBounds(81, 115, 151, 20);
		Produse.add(prod3);
		prod3.setColumns(10);
		
		JLabel lblIdProdus = new JLabel("ID produs");
		lblIdProdus.setBounds(22, 53, 64, 17);
		Produse.add(lblIdProdus);
		
		JLabel lblCantitate = new JLabel("Cantitate");
		lblCantitate.setBounds(22, 84, 46, 14);
		Produse.add(lblCantitate);
		
		JLabel lblPret = new JLabel("Pret");
		lblPret.setBounds(22, 115, 46, 14);
		Produse.add(lblPret);
		
		prod4 = new JTextField();
		prod4.setBounds(81, 146, 151, 20);
		Produse.add(prod4);
		prod4.setColumns(10);
		
		JLabel lblNume = new JLabel("Nume");
		lblNume.setBounds(22, 146, 46, 14);
		Produse.add(lblNume);
		
		
		
		JButton btnBack_2 = new JButton("Back");
		btnBack_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Comanda.setVisible(false);
				Meniu.setVisible(true);
			}
		});
		btnBack_2.setBounds(28, 368, 352, 45);
		Comanda.add(btnBack_2);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(28, 25, 326, 166);
		Comanda.add(scrollPane_2);
		
		table_2 = new JTable();
		scrollPane_2.setViewportView(table_2);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(416, 25, 326, 166);
		Comanda.add(scrollPane_3);
		
		table_3 = new JTable();
		scrollPane_3.setViewportView(table_3);
		
		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(416, 247, 326, 166);
		Comanda.add(scrollPane_4);
		
		table_4 = new JTable();
		scrollPane_4.setViewportView(table_4);
		
		JLabel lblClienti = new JLabel("CLIENTI");
		lblClienti.setBounds(160, 0, 112, 23);
		Comanda.add(lblClienti);
		
		JLabel lblProduse = new JLabel("PRODUSE");
		lblProduse.setBounds(550, 4, 112, 23);
		Comanda.add(lblProduse);
		
		JLabel lblComenzi = new JLabel("COMENZI");
		lblComenzi.setBounds(550, 224, 112, 23);
		Comanda.add(lblComenzi);
		
		cmd1 = new JTextField();
		cmd1.setEditable(false);
		cmd1.setBounds(62, 224, 124, 23);
		Comanda.add(cmd1);
		cmd1.setColumns(10);
		
		JLabel lblCantitate_1 = new JLabel("Client selectat:");
		lblCantitate_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblCantitate_1.setBounds(62, 202, 124, 23);
		Comanda.add(lblCantitate_1);
		
		cmd2 = new JTextField();
		cmd2.setEditable(false);
		cmd2.setColumns(10);
		cmd2.setBounds(62, 258, 124, 23);
		Comanda.add(cmd2);
		
		JLabel lblId_1 = new JLabel("ID");
		lblId_1.setBounds(24, 228, 46, 14);
		Comanda.add(lblId_1);
		
		JLabel lblNume_2 = new JLabel("Nume");
		lblNume_2.setBounds(24, 262, 46, 14);
		Comanda.add(lblNume_2);
		
		JLabel label = new JLabel("");
		label.setBounds(24, 296, 46, 14);
		Comanda.add(label);
		
		cmd3 = new JTextField();
		cmd3.setEditable(false);
		cmd3.setColumns(10);
		cmd3.setBounds(256, 224, 124, 23);
		Comanda.add(cmd3);
		
		JLabel lblProdusSelectat = new JLabel("Produs selectat:");
		lblProdusSelectat.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblProdusSelectat.setBounds(256, 202, 124, 23);
		Comanda.add(lblProdusSelectat);
		
		cmd4 = new JTextField();
		cmd4.setEditable(false);
		cmd4.setColumns(10);
		cmd4.setBounds(256, 258, 124, 23);
		Comanda.add(cmd4);
		
		JLabel label_2 = new JLabel("ID");
		label_2.setBounds(208, 228, 46, 14);
		Comanda.add(label_2);
		
		JLabel label_3 = new JLabel("Nume");
		label_3.setBounds(208, 262, 46, 14);
		Comanda.add(label_3);
		
		cmd5 = new JTextField();
		cmd5.setColumns(10);
		cmd5.setBounds(256, 296, 124, 23);
		Comanda.add(cmd5);
		
		JLabel lblCantitate_2 = new JLabel("Cantitate");
		lblCantitate_2.setBounds(208, 300, 46, 14);
		Comanda.add(lblCantitate_2);
	}
}
