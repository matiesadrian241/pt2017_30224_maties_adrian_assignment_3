package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;

import connection.SQL_connection;
import model.Produs;

public class ProdusDAO {
	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO product (nume,cantitate,pret)"
			+ " VALUES (?,?,?)";
	private static final String updateStatementString = "UPDATE product" +" set nume=?,cantitate=?,pret=? where id_produs=?";
	private static final String deleteStatementString = "DELETE FROM product where id_produs = ?";
	private static final String updateStockStatementString = "UPDATE product" +" set cantitate=? where id_produs=?";
			
	private final static String findStatementString = "SELECT * FROM product where id_produs = ?";
	public static Produs gasesteProdus(int Id) {
		Produs toReturn = null;
		Connection dbConnection = SQL_connection.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1,Id);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("nume");
			int quantity = rs.getInt("cantitate");
			int price = rs.getInt("pret");
			toReturn = new Produs(Id, name, quantity, price);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProdusDAO:findById " + e.getMessage());
		} finally {
			SQL_connection.close(rs);
			SQL_connection.close(findStatement);
			SQL_connection.close(dbConnection);
		}
		return toReturn;
	}
	
	public static int insert(Produs product) {
		Connection dbConnection = SQL_connection.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, product.getNume());
			insertStatement.setInt(2, product.getCantitate());
			insertStatement.setInt(3, product.getPret());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:insert " + e.getMessage());
		} finally {
			SQL_connection.close(insertStatement);
			SQL_connection.close(dbConnection);
		}
		return insertedId;
	}
	public static int update(Produs x) {
		Connection dbConnection = SQL_connection.getConnection();

		PreparedStatement updateStatement = null;
		int insertedId = -1;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, x.getNume());
			updateStatement.setInt(2, x.getCantitate());
			updateStatement.setInt(3, x.getPret());
			updateStatement.setInt(4, x.getId_produs());
			updateStatement.executeUpdate();

			insertedId=1;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:update " + e.getMessage());
		} finally {
			SQL_connection.close(updateStatement);
			SQL_connection.close(dbConnection);
		}
		return insertedId;
	}
	public static int sterge(int id) {
		Connection dbConnection = SQL_connection.getConnection();

		PreparedStatement deleteStatement = null;
		int insertedId = -1;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, id);
			deleteStatement.executeUpdate();
			insertedId=1;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:delete " + e.getMessage());
		} finally {
			SQL_connection.close(deleteStatement);
			SQL_connection.close(dbConnection);
		}
		return insertedId;
	}
	public static int updateStock(int id,int quantity) {
		Connection dbConnection = SQL_connection.getConnection();

		PreparedStatement updateStatement = null;
		int insertedId = -1;
		try {
			updateStatement = dbConnection.prepareStatement(updateStockStatementString, Statement.RETURN_GENERATED_KEYS);
			
			updateStatement.setInt(1, quantity);
			updateStatement.setInt(2, id);
			updateStatement.executeUpdate();

			insertedId=1;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:update " + e.getMessage());
		} finally {
			SQL_connection.close(updateStatement);
			SQL_connection.close(dbConnection);
		}
		return insertedId;
	}
	
	public static List<Object> getAllProducts() {
		List<Object> p = new ArrayList<Object>();
		try {
			Connection dbConnection = SQL_connection.getConnection();
			Statement s = dbConnection.createStatement();
			ResultSet rs = s.executeQuery("select * from product");
			while (rs.next()) {
				Produs cust = new Produs();
				cust.setId_produs(rs.getInt("id_produs"));
				cust.setNume(rs.getString("nume"));
				cust.setCantitate(rs.getInt("cantitate"));
				cust.setPret(rs.getInt("pret"));
				
				p.add(cust);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return p;
	}
}
