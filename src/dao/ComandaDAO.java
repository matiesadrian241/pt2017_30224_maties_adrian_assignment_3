package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import connection.SQL_connection;
import model.Comanda;
public class ComandaDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO comanda (id_client,id_produs,cantitate,pret_total)"
			+ " VALUES (?,?,?,?)";
	private static final String updateStatementString = "UPDATE comanda" +" set pret_total=? where id_client=?";
	public static int insert(Comanda order) {
		Connection dbConnection = SQL_connection.getConnection();
		PreparedStatement insertStatement = null;
		ResultSet rs,last_id = null;
		int insertedId = -1,last_client=-1;
		try{
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setLong(1, order.getId_client());
			insertStatement.setInt(2, order.getId_produs());
			insertStatement.setInt(3, order.getCantitate());
			insertStatement.setInt(4, order.getPret_total());
			insertStatement.executeUpdate();
			if(order.getId_client()==last_client)
				rs=last_id;
			else{
				rs = insertStatement.getGeneratedKeys();}
			last_id=rs;
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
			last_client=order.getId_client();
		} catch (SQLException e){
			LOGGER.log(Level.WARNING, "ProdusDAO:insert " + e.getMessage());
		} finally {
			SQL_connection.close(insertStatement);
			SQL_connection.close(dbConnection);
		}
		return insertedId;
	}
	public static int update(int id,int total) {
		Connection dbConnection = SQL_connection.getConnection();

		PreparedStatement updateStatement = null;
		int insertedId = -1;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setInt(2, id);
			updateStatement.setInt(1, total);
			updateStatement.executeUpdate();
			insertedId=1;
		} catch (SQLException e){
			LOGGER.log(Level.WARNING, "ComandaDAO:update " + e.getMessage());
		} finally {
			SQL_connection.close(updateStatement);
			SQL_connection.close(dbConnection);
		}
		return insertedId;
	}
}
