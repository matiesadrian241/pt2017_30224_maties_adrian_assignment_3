package dao;
import java.awt.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import connection.SQL_connection;
import model.Client;
import java.sql.Connection;
public class ClientDAO {
	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO client (nume,adresa,email,varsta)"
			+ " VALUES (?,?,?,?)";
	private static final String updateStatementString = "UPDATE client" +" set nume=?,adresa=?,email=?,varsta=? where id_client=?";
	private static final String deleteStatementString = "DELETE FROM client where id_client = ?";
			
	private final static String findStatementString = "SELECT * FROM client where id_client = ?";
	public static Client gasireClient_id(int Id) {
		Client toReturn = null;
		Connection dbConnection = SQL_connection.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1,Id);
			rs = findStatement.executeQuery();
			rs.next();
			String name = rs.getString("nume");
			String address = rs.getString("adresa");
			String email = rs.getString("email");
			int varsta =rs.getInt("varsta");
			toReturn = new Client(Id, name, address, email,varsta);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			SQL_connection.close(rs);
			SQL_connection.close(findStatement);
			SQL_connection.close(dbConnection);
		}
		return toReturn;
	}
	
	public static int insert(Client client) {
		Connection dbConnection = SQL_connection.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, client.getNume());
			insertStatement.setString(2, client.getAdresa());
			insertStatement.setString(3, client.getEmail());
			insertStatement.setInt(4, client.getVarsta());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			SQL_connection.close(insertStatement);
			SQL_connection.close(dbConnection);
		}
		return insertedId;
	}
	public static int update(int id,String name,String address, String email, int varsta) {
		Connection dbConnection = SQL_connection.getConnection();

		PreparedStatement updateStatement = null;
		int insertedId = -1;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, name);
			updateStatement.setString(2, address);
			updateStatement.setString(3, email);
			updateStatement.setInt(4, varsta);
			updateStatement.setInt(5, id);
			updateStatement.executeUpdate();
			insertedId=1;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			SQL_connection.close(updateStatement);
			SQL_connection.close(dbConnection);
		}
		return insertedId;
	}
	public static int sterge(int id) {
		Connection dbConnection = SQL_connection.getConnection();
		PreparedStatement deleteStatement = null;
		int insertedId = -1;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, id);
			deleteStatement.executeUpdate();
			insertedId=1;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			SQL_connection.close(deleteStatement);
			SQL_connection.close(dbConnection);
		}
		return insertedId;
	}
}
