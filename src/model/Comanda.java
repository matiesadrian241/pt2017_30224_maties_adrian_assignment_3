package model;

public class Comanda {
	private int id_comanda;
	private int id_client;
	private int id_produs;
	private int cantitate;
	private int pret_total;

public Comanda(int id, int id_client, int id_produs, int cantitate, int pret_total) {
	super();
	this.id_comanda = id;
	this.id_client = id_client;
	this.id_produs = id_produs;
	this.cantitate = cantitate;
	this.pret_total = pret_total;
}
public Comanda(int id_client, int id_produs, int cantitate, int pret_total) {
	super();
	this.id_client = id_client;
	this.id_produs = id_produs;
	this.cantitate = cantitate;
	this.pret_total = pret_total;
}
public int getId_comanda() {
	return id_comanda;
}

public void setId_comanda(int id_comanda) {
	this.id_comanda = id_comanda;
}

public int getId_client() {
	return id_client;
}

public void setId_client(int id_client) {
	this.id_client = id_client;
}

public int getId_produs() {
	return id_produs;
}

public void setId_produs(int id_produs) {
	this.id_produs = id_produs;
}

public int getCantitate() {
	return cantitate;
}

public void setCantitate(int cantitate) {
	this.cantitate = cantitate;
}

public int getPret_total() {
	return pret_total;
}

public void setPret_total(int pret_total) {
	this.pret_total = pret_total;
}

}
