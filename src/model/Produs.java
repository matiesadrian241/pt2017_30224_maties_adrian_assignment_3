package model;

public class Produs {
	private int id_produs;
	private String nume;
	private int cantitate;
	private int pret;
	public Produs(int id_produs, String nume, int cantitate, int pret) {
		super();
		this.id_produs = id_produs;
		this.nume = nume;
		this.cantitate = cantitate;
		this.pret = pret;
	}
	public Produs(String nume, int cantitate, int pret) {
		super();
		this.nume = nume;
		this.cantitate = cantitate;
		this.pret = pret;
	}
	public Produs(){
		
	}
	public int getId_produs() {
		return id_produs;
	}
	public void setId_produs(int id_produs) {
		this.id_produs = id_produs;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public int getCantitate() {
		return cantitate;
	}
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	public int getPret() {
		return pret;
	}
	public void setPret(int pret) {
		this.pret = pret;
	}
	
}
