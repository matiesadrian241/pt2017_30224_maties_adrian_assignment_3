package connection;
import java.sql.*;
import java.util.logging.Level;
public class SQL_connection {
	private static SQL_connection x = new SQL_connection();
	private Connection createConnection() {
		Connection conex = null;
		try {
			conex = DriverManager.getConnection("jdbc:mysql://localhost:3306/cumparaturi","root","acadea15");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conex;
	}
	public static Connection getConnection() {
		return x.createConnection();
	}
	public static void close(Connection conex) {
		if (conex!=null) {
				try {
					conex.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}
	public static void close(ResultSet result) {
		if (result!=null) {
			try {
				result.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public static void close(Statement stat) {
		if (stat!=null) {
			try {
				stat.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
