package bll;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import dao.ClientDAO;
import model.Client;
import validator.Validator;
import validator.ValidareVarsta;
public class ClientBLL {
	private List<Validator<Client>> validators;
	
	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new ValidareVarsta());
	}
	public int insertClient(Client client) {
		for (Validator<Client> a : validators) {
			a.validate(client);
		}
		return ClientDAO.insert(client); 
	}
	public int updateClient(int id,String nume, String adresa, String email,int varsta) {
		
		return ClientDAO.update(id,nume,adresa,email,varsta); 
	}
	public int stergeClient(int id) {
		return ClientDAO.sterge(id); 
	}
	public Client gasireClient(int id) {
		Client x=ClientDAO.gasireClient_id(id);
		if (x==null) {
			throw new NoSuchElementException("Clientul cu id=" + id + " nu a fost gasit!");
		}
		return x;
	}
}
