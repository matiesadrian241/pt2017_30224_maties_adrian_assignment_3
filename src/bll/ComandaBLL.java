package bll;
import dao.ComandaDAO;
import model.Comanda;
public class ComandaBLL {
	public int inserareComanda(Comanda comanda) {
		return ComandaDAO.insert(comanda); 
	}
	public int updateOrder(int id,int pret) {
		return ComandaDAO.update(id,pret); 
	}
}
