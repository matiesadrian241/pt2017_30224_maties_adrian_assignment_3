package bll;
import java.util.List;
import java.util.NoSuchElementException;
import dao.ProdusDAO;
import model.Produs;

public class ProdusBLL {
	public int inserareProdus(Produs product) {
		return ProdusDAO.insert(product); 
	}
	public int updateProdus(Produs x) {
		
		return ProdusDAO.update(x); 
	}
	public int stergeProdus(int id){
		return ProdusDAO.sterge(id); 
	}
	public Produs gasireProdus(int id) {
		Produs x=ProdusDAO.gasesteProdus(id);
		if (x==null) {
			throw new NoSuchElementException("produsul cu id =" + id + " nu a fost gasit!");
		}
		return x;
	}
	public int updateCantitate(int id, int price) {
		return ProdusDAO.updateStock(id,price); 
	}
}
